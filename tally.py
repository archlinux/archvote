#!/usr/bin/env python3

import argparse
import collections
import copy
import gnupg
import itertools
import os
import sys
import configparser


def tally(candidates, votes, tie_order, colwidth):
    # determine the total number of voters
    numvotes = len(votes)

    # count the first preference of each voter
    counter = collections.Counter(dict(zip(candidates, itertools.repeat(0))))
    counter.update([vote[0] for vote in votes])

    # check whether a candidate holds a majority
    if max(counter.values()) > numvotes // 2:
        # print summary table and return winner
        winner = max(counter, key=counter.get)
        print_table(counter, numvotes, winner, None)
        return winner

    # determine the candidate who holds the fewest first preferences
    minvotes = min(counter.values())
    low = set([key for key in counter if counter[key] == minvotes])
    if len(low) == 1:
        candidate = low.pop()
    else:
        # resolve ties using the tie-breaking order
        for candidate in reversed(tie_order):
            if candidate in low:
                break

    # eliminate the candidate who holds the fewest first preferences
    candidates.remove(candidate)
    for vote in votes:
        vote.remove(candidate)

    # print summary table and return None
    print_table(counter, numvotes, None, candidate)
    return None


def print_table(counter, numvotes, winner, rem):
    for key, val in sorted(counter.items(), key=lambda x: x[1], reverse=True):
        percentage = val / numvotes * 100
        if key == winner:
            # highlight the winner in bold green
            print('\033[92m\033[1m', end = '')
        elif key == rem:
            # highlight eliminated candidates in red
            print('\033[91m', end = '')
        username = key.ljust(colwidth)
        print('{} {} votes ({:.2f}%)\033[0m'.format(username, val, percentage))


parser = argparse.ArgumentParser(description='Arch Linux voting tool')
parser.add_argument('--votes', default='votes', help='Directory containing the votes')
parser.add_argument('--verbose', action='store_true', help='Print exact tie-breaking order by disclosing the first ballot')
parser.add_argument('config', help='File containing the election configuration', type=argparse.FileType('r'))
args = parser.parse_args()

votesdir = args.votes
config_file = args.config

config = configparser.ConfigParser(allow_no_value=True)
config.read_file(config_file)
identifier = config.get("election", "identifier")
seats = int(config.get("election", "seats"))
election_type = "instant-runoff"
candidates = list(config['candidates'].keys())
candidates.sort()
if seats >= len(candidates):
    election_type = "confirmation"

# parse votes
votes = []
users = set()
for fn in sorted(os.listdir(votesdir)):
    if not fn.endswith('.asc'):
        continue
    with open(os.path.join(votesdir, fn)) as f:
        # extract message body
        gpg = gnupg.GPG()
        data = str(gpg.decrypt(f.read()))

        config = configparser.ConfigParser(allow_no_value=True)
        config.read_string(data)

        vote_identifier = config.get("election", "identifier")
        if vote_identifier != identifier:
            print(f'wrong election identifier, expected "{identifier}"', file=sys.stderr)
            sys.exit(1)

        username = config.get("voter", "username")
        if username in users:
            print('duplicate vote: {}'.format(username), file=sys.stderr)
            sys.exit(1)
        users.add(username)

        # collect votes
        current_votes = list(config["votes"].keys())
        for vote in current_votes:
            if vote not in candidates:
                print('unknown candidate: {}'.format(vote), file=sys.stderr)
                sys.exit(1)
        if election_type == "instant-runoff":
            if sorted(current_votes) != candidates:
                print('missing some candidates from instant-runoff vote: {}'.format(vote), file=sys.stderr)
                sys.exit(1)
        votes.append(current_votes)

# display headers
print('{}'.format(identifier))
print('{} voting with {} voters done for {} seats...'.format(election_type, len(votes), seats))
print('Candidates: {}'.format(', '.join(candidates)))
if args.verbose:
    print('Tie-breaking order: {}'.format(', '.join(votes[0])))

winners = []
colwidth = max(len(candidate) for candidate in candidates) + 2

if election_type == "instant-runoff":
    for seat in range(1, seats + 1):
        # copy remaining cadidates and votes before tally modifies them
        seat_candidates = copy.deepcopy(candidates)
        seat_votes = copy.deepcopy(votes)

        # tally votes and announce winner
        for n in range(len(seat_votes[0])):
            heading = 'Seat {} - Round {}'.format(seat, n + 1)
            print()
            print(heading)
            print('-' * len(heading))

            winner = tally(seat_candidates, seat_votes, seat_votes[0], colwidth)
            if winner:
                winners.append(winner)
                # remove the winner for the next seat
                candidates.remove(winner)
                for vote in votes:
                    vote.remove(winner)
                print()
                break
elif election_type == "confirmation":
    numvotes = len(votes)
    majority = (numvotes // 2) + 1
    counter = collections.Counter(list(itertools.chain.from_iterable(votes)))

    print("A majority of {} votes is required per candidate".format(majority))
    for candidate, candidate_votes in sorted(counter.items()):
        heading = 'Candidate {}'.format(candidate)
        print()
        print(heading)
        print('-' * len(heading))

        percentage = candidate_votes / numvotes * 100
        if candidate_votes >= majority:
            winners.append(candidate)
            # highlight the winner in bold green
            print('\033[92m\033[1m', end = '')
        else:
            # highlight eliminated candidates in red
            print('\033[91m', end = '')
        print('{} votes ({:.2f}%)\033[0m'.format(candidate_votes, percentage))
    print()


heading = 'Winners'
print()
print(heading)
print('-' * len(heading))
for winner in winners:
    print(winner)
