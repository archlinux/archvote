CONFIG?=conf/PUT_VALID_ELECTION_SETTINGS_FILENAME_HERE.ini

tally: verify
	@echo
	@python tally.py $(CONFIG)

votes/%.asc: FORCE
	@python verify.py $@ $(CONFIG) conf/all-users.csv

verify: $(wildcard votes/*.asc)

FORCE:

.PHONY: FORCE tally verify
