#!/usr/bin/env python3

import csv
import gnupg
import argparse
import sys
import configparser


# parse command line arguments
parser = argparse.ArgumentParser(description='Arch Linux voting verification tool')
parser.add_argument('votesfile', help='File containing the vote', type=argparse.FileType('r'))
parser.add_argument('config', help='File containing the election settings', type=argparse.FileType('r'))
parser.add_argument('userdb', help='File containing the userdb', type=argparse.FileType('r'))
args = parser.parse_args()

fn = args.votesfile
configfn = args.config
usersfn = args.userdb

config = configparser.ConfigParser(allow_no_value=True)
config.read_file(configfn)

# parse election settings
identifier = config.get("election", "identifier")
seats = int(config.get("election", "seats"))
candidates = list(config['candidates'].keys())
candidates.sort()
election_type = "instant-runoff"
if seats >= len(candidates):
    election_type = "confirmation"

# parse user database
users = {}
usersreader = csv.reader(usersfn)
for row in usersreader:
    if len(row) != 4:
        print('{}: invalid format'.format(usersfn), file=sys.stderr)
        sys.exit(1)
    users[row[0].lower()] = row[1:]

# check candidates database
for candidate in candidates:
    if candidate not in users:
        print('{}: invalid user name: {}'.format(configfn, candidate), file=sys.stderr)
        sys.exit(1)

# parse votes file
errors = []

# verify signature and obtain public key fingerprint
gpg = gnupg.GPG()
verified = gpg.verify(fn.read())
if not verified or not verified.valid:
    errors.append('invalid signature ({})'.format(verified.status))
fingerprint = verified.pubkey_fingerprint

# rewind file and extract message body
fn.seek(0)
data = str(gpg.decrypt(fn.read()))

try:
    config = configparser.ConfigParser(allow_no_value=True)
    config.read_string(data)

    vote_identifier = config.get("election", "identifier")
    if vote_identifier != identifier:
        errors.append(f'wrong election identifier, expected "{identifier}"')

    username = config.get("voter", "username").lower()
    if username not in users:
        errors.append('invalid username: {}'.format(username))

    email = config.get("voter", "email")
    if username in users and users[username][1] != email:
        errors.append('email address mismatch: {}'.format(email))

    if username in users and users[username][2] != fingerprint:
        errors.append('fingerprint mismatch: {}'.format(fingerprint))

    votes = list(config["votes"].keys())
    for vote in votes:
        if vote not in candidates:
            errors.append('unknown candidate: {}'.format(vote))
    if election_type == "instant-runoff":
        if sorted(votes) != candidates:
            errors.append('missing some candidates from instant-runoff vote: {}'.format(vote))
except Exception as e:
    errors.append(e)

# print status message and exit
print('verifying {}...'.format(fn.name), end=' ')
if errors:
    for errmsg in errors:
        print('{}: {}'.format(fn, errmsg), file=sys.stderr)
    print('[\033[91mnot ok\033[0m]')
    sys.exit(1)
else:
    print('[\033[92m\033[1mok\033[0m]')
    sys.exit(0)
