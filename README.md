# Arch Linux vote tool

## Voting

### Election setup

Use the [conf/template.ini](conf/template.ini) template for setting up a new election.
This configuration defines an identifier, the number of seats that will get elected
as well as the list of candidates.

In cases where there are less or equal the amount of candidates and seats, a
confirmation vote will be used. In case there are more candidates than seats,
an instant-runoff vote will be used.

The election setup file must be distributed to all eligible voters upfront, to
allow them to verify the ballot.

Example configuration for a vote of 3 seats and 3 candidates that will result
in a confirmation vote:

```ini
[election]
identifier = Arch Linux Something election 3024
seats = 3

[candidates]
candidate1
candidate2
candidate3
```

Example configuration for a vote of 1 seat and 2 candidates that will result in
an instant-runoff vote:

```ini
[election]
identifier = Arch Linux Something election 3024
seats = 1

[candidates]
candidate1
candidate2
```

### Casting a vote

1. To cast a vote, use the [votes/template.ini](votes/template.ini) file as a
   template and copy it with the filename matching your archweb user name.
1. Edit the required fields according to your information as set up in your
   ArchWeb profile as well as the election identifier as defined in the
   election setup. Please note that, with the exception of usernames in the [voter]
   and [votes] sections, all fields are case sensitive.
   - election identifier as defined in the election setup file
   - username as defined in your ArchWeb profile
   - email address as defined in your ArchWeb profile
1. Follow the next subsection based on the voting type of the current vote,
   either instant-runoff or confirmation vote.

### Instant-runoff vote

Starting with the `[votes]` section, list the user names of all candidates as
defined in the election setup, ordered by preference. The user names listed
first have higher priority.

Example:

```ini
[election]
identifier = Arch Linux Something election 3024

[voter]
username = Username-as-in-ArchWeb
email    = foobar@archlinux.org

[votes]
candidate3
candidate1
candidate2
```

### Confirmation vote

Starting with the `[votes]` section, list the user names of all candidates as
defined in the election setup, list the user names of all candidates you want
to vote for and subsequently confirm, and remove any candidates you do not
intend to confirm. Specifying no names means to reject all candidates. The
order of the candidates does not matter.

Example that confirms `candidate1` and `candidate3` but rejects `candidate2`:

```ini
[election]
identifier = Arch Linux Something election 3024

[voter]
username = Username-as-in-ArchWeb
email    = foobar@archlinux.org

[votes]
candidate3
candidate1
```

### Cast your vote

Finally, use `gpg --clearsign` to sign the ballot file. You must use the public
key listed in archweb (or a signing subkey of that key). The appropriate
identity can be selected with `--default-key <key-id>!` The resulting file has
an `.asc` extension and can be submitted to the current Project Leader or the
defined election assistants via email:

```bash
gpg --armor --default-key <key-id>! --clearsign votes/yourusername.ini
```

Please make sure you verify your vote file before submitting it, as described
below.

## Verifying Vote Files

Add all vote files (with an `.asc` extension) to the `votes` subdirectory. Then
run `make verify` from the top-level directory of this project. The current
election settings can be passed via the `CONFIG` option.

```bash
make verify CONFIG=conf/mediator-team-3094.ini
```

Or by running the [verify.py](verify.py) file directly:

```bash
./verify.py votes/foobar.asc conf/mediator-team-3094.ini conf/all-users.csv
```

## Tallying

Make sure that the vote file of the current Project Leader (which will be used
for breaking ties) comes first in alphabetical order. For example, you can
prefix that file by an underscore and make sure all other file names start with
a lowercase letter.

Run `make` and pass the election settings via the `CONFIG` option.

```bash
make CONFIG=conf/mediator-team-3094.ini
```

## User Database

The `conf/all-users.csv` file should be generated from archweb with the
`export-archweb-users.py` script on archlinux.org. If the currently defined
data is out of date, please adjust your ArchWeb profile.
